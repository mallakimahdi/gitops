#!/bin/bash

set -xe

export SERVER_PUBLIC_IP="1.2.3.4"

apt update
apt install docker.io -y
systemctl enable docker
systemctl start docker

mkdir -p /opt/rke-installer
cd /opt/rke-installer

wget https://github.com/rancher/rke/releases/download/v1.3.2/rke_linux-amd64 -O rke

chmod +x rke
sudo mv rke /usr/bin

ssh-keygen -t rsa -N '' -f ~/.ssh/id_rsa <<< y
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

cat > cluster.yml << EOF
nodes:
    - address: $SERVER_PUBLIC_IP
      user: root
      role:
        - controlplane
        - etcd
        - worker
EOF

rke up

mkdir ~/.kube
cp kube_config_cluster.yml ~/.kube/config

curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x kubectl
mv kubectl /usr/bin

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh

kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/v0.0.22/deploy/local-path-storage.yaml

kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
